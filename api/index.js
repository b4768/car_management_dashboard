const express = require("express");
const router = express.Router();
const carRouter = require("./car");

router.use("/car", carRouter);

module.exports = router;
