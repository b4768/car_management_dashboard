const express = require("express");
const router = express.Router();
const carRouter = require("./car");

router.get("/", (req, res) => {
  res.render("layouts/dashboard");
});
router.use("/car", carRouter);

module.exports = router;
