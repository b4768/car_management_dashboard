const express = require("express");
const router = express.Router();
const {
  list,
  create,
  update,
  destroy,
  add,
  index,
} = require("../controllers/carController");
const validate = require("../middleware/validate");
const { createCarRules } = require("../validators/rule");

router.get("/", index);
router.get("/list", list);
router.get("/create", add);
router.post("/create", validate(createCarRules), create);
router.put("/update", update);
router.delete("/delete/:id", destroy);

module.exports = router;
