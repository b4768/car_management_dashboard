const { body } = require("express-validator");

const createCarRules = [
  body("name").notEmpty().withMessage("name is required"),
  body("type").notEmpty().withMessage("type is required"),
  body("price").notEmpty().withMessage("price is required"),
];

module.exports = {
  createCarRules,
};
