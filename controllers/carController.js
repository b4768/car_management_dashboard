const model = require("../models");

module.exports = {
  index: async (req, res) => {
    try {
      return res.render("pages/car/index");
    } catch (error) {}
  },
  list: async (req, res) => {
    try {
      const datas = await model.car.findAll();

      return res.render("pages/car/list", { datas });
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
  add: (req, res) => {
    try {
      return res.render("pages/car/create");
    } catch (error) {}
  },
  create: async (req, res) => {
    try {
      const data = await model.car.create(req.body);

      return res.redirect("/car/list");
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
  update: async (req, res) => {
    try {
      const data = await model.car.update(
        {
          name: req.body.name,
          type: req.body.type,
          price: req.body.price,
          path: req.body.path,
        },
        {
          where: {
            id: req.body.id,
          },
        }
      );

      return res.status(200).json({
        success: true,
        error: 0,
        message: "data successfully updated",
        data: data,
      });
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
  destroy: async (req, res) => {
    try {
      const data = await model.car.destroy({
        where: {
          id: req.params.id,
        },
      });

      return data;
    } catch (error) {
      return res.status(500).json({
        success: false,
        error: error.code,
        message: error,
        data: null,
      });
    }
  },
};
